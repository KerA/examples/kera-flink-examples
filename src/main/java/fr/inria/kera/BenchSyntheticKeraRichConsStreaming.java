/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.util.serialization.TypeInformationKeyValueSerializationSchema;
import org.apache.flink.util.Collector;

import fr.inria.kera.connectors.flink.FlinkKeraConsumer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;

/**
 * Created by omarcu on 13/10/17.
 */
public class BenchSyntheticKeraRichConsStreaming {

	public static void main(String[] args) throws Exception {

		final ParameterTool parameterTool = ParameterTool.fromArgs(args);

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.getConfig().enableObjectReuse();
		env.getCheckpointConfig().setCheckpointInterval(1000000);  // in milliseconds -> 1000s
		env.setBufferTimeout(-1);  // throughput optimized
		env.getConfig().setGlobalJobParameters(parameterTool);

		// next is used by source operators
		//numberNodes/numberTaskManagers=localTaskManagers
		//->each source subtask has numParallelSubtasks = getRuntimeContext().getNumberOfParallelSubtasks()
		//numParallelSubtasks/numberTaskManagers gives the local parallelism
		//->local sources share streamlets (competing on groups) but only the first source launches the multiRead RPC with
		//numParallelSubtasks/numberTaskManagers representing the number of partitions of the plasma object store
		
		int numberConsumers = Integer.valueOf(parameterTool.getRequired("numberConsumers"));
		int maxThroughputCons = Integer.valueOf(parameterTool.getRequired("maxThroughputCons"));

		String producerTopic = parameterTool.getRequired("topic");

		Properties consumerProps = new Properties();
		consumerProps.setProperty("READER_ID", "1"); //unique per application
		consumerProps.setProperty("STREAM_ID", parameterTool.getRequired("streamId")); //created before with createTable api kera
		consumerProps.setProperty("RECORDS_COUNT", parameterTool.getRequired("recordsCount"));
		consumerProps.setProperty("NNODES", parameterTool.getRequired("numberNodes"));
		consumerProps.setProperty("NSTREAMLETS", parameterTool.getRequired("nStreamlets"));
		consumerProps.setProperty("NACTIVEGROUPS", parameterTool.getRequired("nActiveGroups"));
		consumerProps.setProperty("BATCH_STREAM", parameterTool.getRequired("batchSize")); //for each group of a streamlet
		consumerProps.setProperty("locator", parameterTool.getRequired("locator"));
		consumerProps.setProperty("clusterName", parameterTool.getRequired("clusterName"));
		consumerProps.setProperty("NRECORDS_THROTTLE", parameterTool.getRequired("nrecordsThrottle"));
		consumerProps.setProperty("TOTAL_OBJECTS_SIZE", parameterTool.getRequired("totalObjectsSize")); //shared plasma store

		//not used here
		HashMap<Integer, String> inputFiles = new HashMap<>();

		File[] inputs = new File(parameterTool.getRequired("inputdir")).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String name) {
				boolean accept = name.contains("enwiki");
				return accept;
			}
		});

		int inputIndex = 0;
		for (File input : inputs) {
			inputFiles.put(inputIndex++, input.getAbsolutePath());
		}

		//partition by key
		//each record is a tuple<string,string> with key="prodId timestamp"

		if (numberConsumers >= 1) {
			String slotSourceCons = "src-cons" + "_" + new Random().nextInt(1000000);
			String slotSourceProc = "src-proc" + "_" + new Random().nextInt(1000000);

			TypeInformationKeyValueSerializationSchema<byte[], byte[]> schema =
					new TypeInformationKeyValueSerializationSchema<byte[], byte[]>(
							byte[].class, byte[].class, env.getConfig());

			FlinkKeraConsumer<Tuple2<byte[], byte[]>> keraConsumer =
					new FlinkKeraConsumer<Tuple2<byte[], byte[]>>(producerTopic, schema, consumerProps);
			keraConsumer.setCommitOffsetsOnCheckpoints(false);

			DataStream<Tuple2<byte[], byte[]>> mycons = env.addSource(keraConsumer)
					.name("sourceCons")
					.setParallelism(numberConsumers)
					.slotSharingGroup(slotSourceCons);
			
			mycons.flatMap(new RichThroughputLogger<Tuple2<byte[], byte[]>>(maxThroughputCons, "consumer"))
					.name("ThroughputLoggerCons")
					.setParallelism(numberConsumers)
					.slotSharingGroup(slotSourceProc)
					.writeAsText(inputFiles.get(0) + "out", FileSystem.WriteMode.OVERWRITE)
					.name("sinkCons")
					.setParallelism(1)
					.slotSharingGroup(slotSourceProc);
		}


		env.execute("flink-consumer-1");
	}

	public static final class Tokenizer implements FlatMapFunction<String, String> {
		private static final long serialVersionUID = 1L;

		@Override
		public void flatMap(String value, Collector<String> out) throws Exception {
			// normalize and split the line
			String[] tokens = value.split(" ");

			// emit words
			for (String token : tokens) {
				out.collect(token);
			}
		}
	}
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by omarcu on 11/07/17.
 */
public abstract class LoadGeneratorSource<T> extends RichParallelSourceFunction<T> {

	private static final Logger LOG = LoggerFactory.getLogger(LoadGeneratorSource.class);

	private boolean running = true;

	private final int loadTargetHz;
	private final int timeSliceLengthMs;

	public LoadGeneratorSource(int loadTargetHz, int timeSliceLengthMs) {
		this.loadTargetHz = loadTargetHz;
		this.timeSliceLengthMs = timeSliceLengthMs;
	}

	/**
	 * Subclasses must override this to generate a data element
	 */
	public abstract T generateElement();

	/**
	 * The main loop
	 */
	@Override
	public void run(SourceContext<T> sourceContext) throws Exception {
		int elements = loadPerTimeslice();

		long emitStartTime = System.currentTimeMillis();

		for (int i = 0; i < elements; i++) {
			sourceContext.collect(generateElement());
			if (!running) {
				break;
			}
		}

		LOG.info("Source Throughput: " + elements + " in " + (System.currentTimeMillis() - emitStartTime) + "\n");

//		 Sleep for the rest of timeslice if needed
//		long emitTime = System.currentTimeMillis() - emitStartTime;
//			if (emitTime < timeSliceLengthMs) {
//				Thread.sleep(timeSliceLengthMs - emitTime);
//			}
//		}
		sourceContext.close();
	}

	@Override
	public void cancel() {
		running = false;
	}

	/**
	 * Given a desired load figure out how many elements to generate in each timeslice
	 * before yielding for the rest of that timeslice
	 */
	private int loadPerTimeslice() {
		int messagesPerOperator = loadTargetHz; // / getRuntimeContext().getNumberOfParallelSubtasks();
		return messagesPerOperator / (1000 / timeSliceLengthMs);
	}
}

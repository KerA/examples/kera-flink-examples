/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

/**
 * Created by omarcu on 13/10/17.
 */
public class BenchSyntheticProdConsStreaming {

	public static void main(String[] args) throws Exception {

		final ParameterTool parameterTool = ParameterTool.fromArgs(args);

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.getConfig().enableObjectReuse();
		env.getCheckpointConfig().setCheckpointInterval(1000000); //in milliseconds -> 1000s
		env.setBufferTimeout(-1); //throughput optimized

		int numberProducers = Integer.valueOf(parameterTool.getRequired("numberProducers"));
		int numberConsumers = Integer.valueOf(parameterTool.getRequired("numberConsumers"));
		//print every maxThroughputProd
		int maxThroughputProd = Integer.valueOf(parameterTool.getRequired("maxThroughputProd"));
		int maxThroughputCons = Integer.valueOf(parameterTool.getRequired("maxThroughputCons"));
		String producerTopic = parameterTool.getRequired("topic");
		String producerBatchSize = parameterTool.getRequired("batchSize"); //e.g. 16384 bytes - for producer
		Integer batchSize = Integer.valueOf(producerBatchSize) + 50; //extra 50 for request overhead
		producerBatchSize = batchSize.toString();
		String maxPollRecords = parameterTool.getRequired("maxPollRecords"); //100000 - for consumer, large enough to be ignored
		Integer numberPartitions = Integer.valueOf(parameterTool.getRequired("numberPartitions"));
		Integer numberNodes = Integer.valueOf(parameterTool.getRequired("numberNodes"));
		//each request for one node should contain one batch for each partition
		Integer requestSize = Integer.valueOf(producerBatchSize) * numberPartitions / numberNodes;

		//how many records the source should emit (loadTargetHz) every timeSliceLengthMs
		int loadTargetHz = Integer.valueOf(parameterTool.getRequired("loadTargetHz"));
		int timeSliceLengthMs = Integer.valueOf(parameterTool.getRequired("timeSliceLengthMs"));
		int recordSize = Integer.valueOf(parameterTool.getRequired("recordSize"));

		Integer bufferMemory = Integer.valueOf(producerBatchSize) * 1000; //10000 batches
		Integer maxInFlightRequestsPerConnection = 1; //numberPartitions/numberNodes;

		Properties producerProps = new Properties();
		producerProps.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrapServers"));
		producerProps.setProperty("batch.size", producerBatchSize); // config param
		producerProps.setProperty("buffer.memory", bufferMemory.toString()); // should be equiv. CACHE_SIZE=1000 of batchSize records of 100bytes in KERA make it configurable
		producerProps.setProperty("linger.ms", "1"); //wait 1 ms if no single batch of records is prepared
		producerProps.setProperty("retries", "1000");
		producerProps.setProperty("max.in.flight.requests.per.connection", maxInFlightRequestsPerConnection.toString());
		producerProps.setProperty("max.request.size", requestSize.toString()); //can contain multiple batchSize batches
		producerProps.setProperty("receive.buffer.bytes", "-1"); //default 32768 SO_RCVBUF
		producerProps.setProperty("send.buffer.bytes", "-1"); //default 131072 SO_SNDBUF
		producerProps.setProperty("request.timeout.ms", "305000");
		producerProps.setProperty("block.on.buffer.full", "true");

		Properties consumerProps = new Properties();
		consumerProps.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrapServers"));
		consumerProps.setProperty("auto.offset.reset", "earliest");
		consumerProps.setProperty("check.crcs", "false"); //default true - avoid checking checksums
		consumerProps.setProperty("fetch.max.bytes", requestSize.toString()); //per request, each request can get from multiple partitions
		consumerProps.setProperty("fetch.max.wait.ms", "0"); //do not wait if no data found
		consumerProps.setProperty("fetch.min.bytes", "0");
		consumerProps.setProperty("max.partition.fetch.bytes", producerBatchSize); // per partition, equiv. batchSize
		consumerProps.setProperty("max.poll.records", maxPollRecords); //default is 500 config param
		consumerProps.setProperty("receive.buffer.bytes", "-1"); //use SO_RCVBUF default, 65536
		consumerProps.setProperty("send.buffer.bytes", "-1"); //use SO_SNDBUF default, 131072
		consumerProps.setProperty("flink.poll-timeout", "1"); //default 100
		consumerProps.setProperty("enable.auto.commit", "false");

		//not used here
		HashMap<Integer, String> inputFiles = new HashMap<>();

		File[] inputs = new File(parameterTool.getRequired("inputdir")).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String name) {
				boolean accept = name.contains("enwiki");
				return accept;
			}
		});

		int inputIndex = 0;
		for (File input : inputs) {
			inputFiles.put(inputIndex++, input.getAbsolutePath());
		}

		for (int prod = 1; prod <= numberProducers; prod++) {
			String slotSourceProd = "src-prod" + prod + "_" + new Random().nextInt(1000000);
			SourceFunction<String> source = new RandomSource(loadTargetHz, timeSliceLengthMs, recordSize);
			DataStream<String> myprodStream = env.addSource(source)
					.name("sourceProd" + prod).setParallelism(1).slotSharingGroup(slotSourceProd);

			myprodStream.flatMap(new RichThroughputLogger<String>(maxThroughputProd, "prod" + prod))
					.name("ThroughputLoggerProd" + prod).setParallelism(1).slotSharingGroup(slotSourceProd);
			FlinkKafkaProducer kafkaProd = new FlinkKafkaProducer<String>(producerTopic, new SimpleStringSchema(), producerProps,
					Optional.of(new FlinkKafkaKeyPartitioner<String>()));
			myprodStream.addSink(kafkaProd)
					.name("sinkProd" + prod).setParallelism(1).slotSharingGroup(slotSourceProd);
		}

		if (numberConsumers >= 1) {
			String slotSourceCons = "src-cons" + "_" + new Random().nextInt(1000000);
			FlinkKafkaConsumer kafkaCons = new FlinkKafkaConsumer<>(
					producerTopic, new SimpleStringSchema(), consumerProps);
			kafkaCons.setStartFromEarliest();
			kafkaCons.setCommitOffsetsOnCheckpoints(false);
			DataStream<String> mycons = env.addSource(kafkaCons)
					.name("sourceCons").setParallelism(numberConsumers).slotSharingGroup(slotSourceCons);
			mycons.flatMap(new RichThroughputLogger<String>(maxThroughputCons, "consumer"))
					.name("ThroughputLoggerCons").setParallelism(numberConsumers).slotSharingGroup(slotSourceCons)
					.writeAsText(inputFiles.get(0) + "out", FileSystem.WriteMode.OVERWRITE)
					.name("sinkCons").setParallelism(1).slotSharingGroup(slotSourceCons);
		}


		env.execute("MultipleProdCons");
	}

	public static final class Tokenizer implements FlatMapFunction<String, String> {
		private static final long serialVersionUID = 1L;

		@Override
		public void flatMap(String value, Collector<String> out) throws Exception {
			// normalize and split the line
			String[] tokens = value.split(" ");

			// emit words
			for (String token : tokens) {
				out.collect(token);
			}
		}
	}
}

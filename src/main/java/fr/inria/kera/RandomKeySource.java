/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.api.java.tuple.Tuple2;

import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * Created by omarcu on 02/18.
 */
public class RandomKeySource extends LoadGeneratorSource<Tuple2<String,String>> {

	private byte[] payload;
	private byte[] key;
	private byte[] keyFirstPart;

	public RandomKeySource(int loadTargetHz, int timeSliceLengthMs, int recordSize, int keySize, int producerId) {
		super(loadTargetHz, timeSliceLengthMs);
		this.payload = new byte[recordSize];
		for (int i = 0; i < payload.length; ++i)
			payload[i] = 'x';

		this.key = new byte[keySize];
		keyFirstPart = (Integer.valueOf(producerId).toString() + " ").getBytes(StandardCharsets.UTF_8);
		for (int i = 0; i < keySize; ++i) {
			key[i] = (byte)0;
		}
		for (int i = 0; i < keyFirstPart.length; ++i) {
			key[i] = keyFirstPart[i];
		}
	}

	@Override
	public Tuple2<String,String> generateElement() {
		for (int i = keyFirstPart.length; i < key.length; ++i) {
			key[i] = (byte)0;
		}
		byte[] keySecondPart = Long.valueOf(new Date().getTime()).toString().getBytes(StandardCharsets.UTF_8);
		for (int i = 0; i < keySecondPart.length; ++i) {
			key[i+keyFirstPart.length] = keySecondPart[i];
		}
		return new Tuple2<>(new String(key, StandardCharsets.UTF_8), new String(payload, StandardCharsets.UTF_8));
	}
}

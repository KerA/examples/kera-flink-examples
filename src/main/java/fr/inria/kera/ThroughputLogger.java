/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by omarcu on 06/07/17.
 */
public class ThroughputLogger<T> implements FlatMapFunction<T, Integer> {
	private static final Logger LOG = LoggerFactory.getLogger(ThroughputLogger.class);

	private long totalReceived = 0;
	private long lastTotalReceived = 0;
	private long lastLogTimeMs = -1;
	private long logfreq;

	private int id;
	private String client;

	public ThroughputLogger(int id, long logfreq, String client) {
		this.logfreq = logfreq;
		this.id = id;
		this.client = client;
	}

	@Override
	public void flatMap(T element, Collector<Integer> collector) throws Exception {
		totalReceived++;
		if (totalReceived % logfreq == 0) {
			// throughput over entire time
			long now = System.currentTimeMillis();

			// throughput for the last "logfreq" elements
			if (lastLogTimeMs == -1) {
				// init (the first)
				lastLogTimeMs = now;
				lastTotalReceived = totalReceived;
			} else {
				long timeDiff = now - lastLogTimeMs;
				long elementDiff = totalReceived - lastTotalReceived;
				double ex = (1000 / (double) timeDiff);
				LOG.info("For {} ID={} During the last {} ms, we received {} elements. " +
								"That's {} elements/second/core.",
						client,id, timeDiff, elementDiff,
						elementDiff * ex );
				// reinit
				lastLogTimeMs = now;
				lastTotalReceived = totalReceived;
			}
		}
	}
}

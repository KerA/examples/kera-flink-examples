/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by omarcu on 13/10/17.
 */
public class BenchProdConsStreaming {

	public static void main(String[] args) throws Exception {

		final ParameterTool parameterTool = ParameterTool.fromArgs(args);

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setBufferTimeout(-1); //throughput optimized

		int numberProducers = Integer.valueOf(parameterTool.getRequired("numberProducers")); //10
		int numberConsumers = Integer.valueOf(parameterTool.getRequired("numberConsumers")); //10
		int maxThroughputProd = Integer.valueOf(parameterTool.getRequired("maxThroughputProd"));     //1000K
		int maxThroughputCons = Integer.valueOf(parameterTool.getRequired("maxThroughputCons"));     //1000K
		String producerTopic = parameterTool.getRequired("topic");
		String producerBatchSize = parameterTool.getRequired("batchSize"); //16384 - for producer
		String maxPollRecords = parameterTool.getRequired("maxPollRecords"); //1000 - for consumer

		Properties producerProps = new Properties();
		producerProps.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrapServers"));
		producerProps.setProperty("batch.size", producerBatchSize); // config param
		producerProps.setProperty("buffer.memory", "67108864");
		producerProps.setProperty("linger.ms", "0");
		producerProps.setProperty("retries", "0");
		producerProps.setProperty("max.in.flight.requests.per.connection", "1");
		producerProps.setProperty("max.request.size", "4194304");
		producerProps.setProperty("receive.buffer.bytes", "-1"); //default 32768 SO_RCVBUF
		producerProps.setProperty("send.buffer.bytes", "-1"); //default 131072 SO_SNDBUF

		Properties consumerProps = new Properties();
		consumerProps.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrapServers"));
		consumerProps.setProperty("auto.offset.reset", "earliest");
		consumerProps.setProperty("check.crcs", "true");
		consumerProps.setProperty("fetch.max.bytes", "8388608"); //per request 8MB
		consumerProps.setProperty("fetch.max.wait.ms", "100"); //wait 100ms if at least 10 bytes not found
		consumerProps.setProperty("fetch.min.bytes", "10");
		consumerProps.setProperty("max.partition.fetch.bytes", "4194304"); //1MB per partition
		consumerProps.setProperty("max.poll.records", maxPollRecords); //default is 500 config param
		consumerProps.setProperty("receive.buffer.bytes", "-1"); //use SO_RCVBUF default, 65536
		consumerProps.setProperty("send.buffer.bytes", "-1"); //use SO_SNDBUF default, 131072
		consumerProps.setProperty("flink.poll-timeout", "5"); //default 100


		HashMap<Integer, String> inputFiles = new HashMap<>(); //todo init

		File[] inputs = new File(parameterTool.getRequired("inputdir")).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String name) {
				boolean accept = name.contains("enwiki");
				return accept;
			}
		});

		int inputIndex = 0;
		for(File input:inputs) {
			inputFiles.put(inputIndex++, input.getAbsolutePath());
		}

		for (int prod = 0; prod < numberProducers; prod++) {
			String slotSourceProd = "src-prod" + prod;
			String slotSinkProd = "sink-prod" + prod;
			DataStream<String> myprod = env.readTextFile(inputFiles.get(prod))
					.name("sourceProd" + prod).setParallelism(1).slotSharingGroup(slotSourceProd)
					.flatMap(new Tokenizer())
					.name("parseLinesProd" + prod).setParallelism(1).slotSharingGroup(slotSourceProd);
			myprod.flatMap(new ThroughputLogger<String>(prod, maxThroughputProd, "prod"))
					.name("ThroughputLoggerProd"+prod).setParallelism(1).slotSharingGroup(slotSourceProd);
			myprod.addSink(new FlinkKafkaProducer<>(producerTopic, new SimpleStringSchema(), producerProps))
					.name("sinkProd"+prod).setParallelism(1).slotSharingGroup(slotSinkProd);
		}

		for (int cons=0; cons < numberConsumers; cons++) {
			String slotSourceCons = "src-cons" + cons;
			String slotSinkCons = "sink-cons" + cons;
			DataStream<String> mycons = env.addSource(new FlinkKafkaConsumer<>(
					producerTopic, new SimpleStringSchema(), consumerProps))
					.name("sourceCons" + cons).setParallelism(1).slotSharingGroup(slotSourceCons);
			mycons.flatMap(new ThroughputLogger<String>(cons+1000, maxThroughputCons, "client"))
					.name("ThroughputLoggerCons" + cons).setParallelism(1).slotSharingGroup(slotSourceCons)
					.writeAsText(inputFiles.get(cons)+"out", FileSystem.WriteMode.OVERWRITE)
					.name("sinkCons"+cons).setParallelism(1).slotSharingGroup(slotSinkCons);

		}

		env.execute("MultipleProdCons");
	}

	public static final class Tokenizer implements FlatMapFunction<String, String> {
		private static final long serialVersionUID = 1L;

		@Override
		public void flatMap(String value, Collector<String> out) throws Exception {
			// normalize and split the line
			String[] tokens = value.split(" ");

			// emit words
			for (String token : tokens) {
				out.collect(token);
			}
		}
	}
}

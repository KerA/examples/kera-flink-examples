/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import java.util.Random;

/**
 * Created by omarcu on 11/07/17.
 */
public class RandomSource extends LoadGeneratorSource<String> {

	private final int recordSize;

	private final Random random;
	private byte[] payload;

	public RandomSource(int loadTargetHz, int timeSliceLengthMs, int recordSize) {
		super(loadTargetHz, timeSliceLengthMs);
		this.recordSize = recordSize;
		this.random = new Random(0);
		this.payload = new byte[recordSize];
		for (int i = 0; i < payload.length; ++i)
			payload[i] = 'x'; //(byte) (random.nextInt(26) + 65);
	}

	@Override
	public String generateElement() {
		return new String(payload);
	}
}

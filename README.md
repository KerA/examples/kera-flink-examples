# kera-flink-examples

This repository implements a [Apache Flink](http://flink.apache.org/) project running KerA streaming examples.

## Compilation

Compile the project as follows:

```bash
mvn clean package -DskipTests -Dcheckstyle.skip=true
```

Run the project as follows:

```bash
mvn exec:java -Dexec.mainClass=fr.inria.kera.BenchSyntheticKeraPushRichConsStreaming
```

## Examples

### flink-consumer-1

Main class: `BenchSyntheticKeraRichConsStreaming.java`.

A complete multi-threaded Flink consumer leveraging a batch pool.

### flink-consumer-2

Main class: `BenchSyntheticKeraPushRichConsStreaming.java`.

A complete multi-threaded Flink consumer leveraging a batch pool as well as the Plasma shared memory.
